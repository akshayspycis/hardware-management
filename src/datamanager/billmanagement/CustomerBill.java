package datamanager.billmanagement;

import java.util.ArrayList;

public class CustomerBill {
    
    String billid = null;

    String customername = null;
    String customercontactno = null;
    String address = null;
    String recaddress = null;
    String city = null;
    String state = null;
    String drivername = null;
    String drivercontactno = null;
    String vehicleno = null;
    String other = null;    
    String transportcharges = null;    
    String bookingdate = null;    
    String bolts = null;
    String returnbolts = null;
    String boltscharges = null;    
    String pane = null;
    String returnpane = null;
    String panecharges = null;    
    String totalitem = null;
    String totalreturnitem = null;
    String totalbalanceitem = null;
    String totalamount = null;
    String discount = null;
    String payableamount = null;
    String paidamount = null;
    String balanceamount = null;
    
    ArrayList<Purchasetable> purchasetable = null;
    ArrayList<Returntable> returntable = null;

    public String getBillid() {
        return billid;
    }

    public void setBillid(String billid) {
        this.billid = billid;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomercontactno() {
        return customercontactno;
    }

    public void setCustomercontactno(String customercontactno) {
        this.customercontactno = customercontactno;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRecaddress() {
        return recaddress;
    }

    public void setRecaddress(String recaddress) {
        this.recaddress = recaddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDrivername() {
        return drivername;
    }

    public void setDrivername(String drivername) {
        this.drivername = drivername;
    }

    public String getDrivercontactno() {
        return drivercontactno;
    }

    public void setDrivercontactno(String drivercontactno) {
        this.drivercontactno = drivercontactno;
    }

    public String getVehicleno() {
        return vehicleno;
    }

    public void setVehicleno(String vehicleno) {
        this.vehicleno = vehicleno;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getTransportcharges() {
        return transportcharges;
    }

    public void setTransportcharges(String transportcharges) {
        this.transportcharges = transportcharges;
    }

    public String getBookingdate() {
        return bookingdate;
    }

    public void setBookingdate(String bookingdate) {
        this.bookingdate = bookingdate;
    }

    public String getBolts() {
        return bolts;
    }

    public void setBolts(String bolts) {
        this.bolts = bolts;
    }

    public String getReturnbolts() {
        return returnbolts;
    }

    public void setReturnbolts(String returnbolts) {
        this.returnbolts = returnbolts;
    }

    public String getBoltscharges() {
        return boltscharges;
    }

    public void setBoltscharges(String boltscharges) {
        this.boltscharges = boltscharges;
    }

    public String getPane() {
        return pane;
    }

    public void setPane(String pane) {
        this.pane = pane;
    }

    public String getReturnpane() {
        return returnpane;
    }

    public void setReturnpane(String returnpane) {
        this.returnpane = returnpane;
    }

    public String getPanecharges() {
        return panecharges;
    }

    public void setPanecharges(String panecharges) {
        this.panecharges = panecharges;
    }

    public String getTotalitem() {
        return totalitem;
    }

    public void setTotalitem(String totalitem) {
        this.totalitem = totalitem;
    }

    public String getTotalreturnitem() {
        return totalreturnitem;
    }

    public void setTotalreturnitem(String totalreturnitem) {
        this.totalreturnitem = totalreturnitem;
    }

    public String getTotalbalanceitem() {
        return totalbalanceitem;
    }

    public void setTotalbalanceitem(String totalbalanceitem) {
        this.totalbalanceitem = totalbalanceitem;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPayableamount() {
        return payableamount;
    }

    public void setPayableamount(String payableamount) {
        this.payableamount = payableamount;
    }

    public String getPaidamount() {
        return paidamount;
    }

    public void setPaidamount(String paidamount) {
        this.paidamount = paidamount;
    }

    public String getBalanceamount() {
        return balanceamount;
    }

    public void setBalanceamount(String balanceamount) {
        this.balanceamount = balanceamount;
    }

    public ArrayList<Purchasetable> getPurchasetable() {
        return purchasetable;
    }

    public void setPurchasetable(ArrayList<Purchasetable> purchasetable) {
        this.purchasetable = purchasetable;
    }

    public ArrayList<Returntable> getReturntable() {
        return returntable;
    }

    public void setReturntable(ArrayList<Returntable> returntable) {
        this.returntable = returntable;
    }
}
