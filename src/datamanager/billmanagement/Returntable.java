package datamanager.billmanagement;

public class Returntable {

    String billid = null;
    String item = null;
    String size = null;
    String quantity = null;
    String rate = null;
    String returndate = null;
    String noofdays = null;
    String returnitem = null;
    String balanceitem = null;
    String amount = null;

    public String getBillid() {
        return billid;
    }

    public void setBillid(String billid) {
        this.billid = billid;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getReturndate() {
        return returndate;
    }

    public void setReturndate(String returndate) {
        this.returndate = returndate;
    }

    public String getNoofdays() {
        return noofdays;
    }

    public void setNoofdays(String noofdays) {
        this.noofdays = noofdays;
    }

    public String getReturnitem() {
        return returnitem;
    }

    public void setReturnitem(String returnitem) {
        this.returnitem = returnitem;
    }

    public String getBalanceitem() {
        return balanceitem;
    }

    public void setBalanceitem(String balanceitem) {
        this.balanceitem = balanceitem;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
