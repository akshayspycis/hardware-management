package datamanager.billmanagement;

public class CustomerPayment {
    
    String customerpaymentid = null;
    String customerid = null;
    String date = null;
    String payment = null;
    String paidby = null;
    String balance = null;

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }    

    public String getCustomerpaymentid() {
        return customerpaymentid;
    }

    public void setCustomerpaymentid(String customerpaymentid) {
        this.customerpaymentid = customerpaymentid;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getPaidby() {
        return paidby;
    }

    public void setPaidby(String paidby) {
        this.paidby = paidby;
    }
}
