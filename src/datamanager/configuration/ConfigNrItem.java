package datamanager.configuration;

public class ConfigNrItem {
    
    String item = null;
    String rate = null;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    
}
