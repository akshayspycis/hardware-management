package datamanager.configuration;

public class ConfigItem {
    
    String item = "";
    String type = "";
    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    

}
