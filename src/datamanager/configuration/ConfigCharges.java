package datamanager.configuration;

public class ConfigCharges {
    
    String boltscharge = null;
    String panecharge = null;

    public String getBoltscharge() {
        return boltscharge;
    }

    public void setBoltscharge(String boltscharge) {
        this.boltscharge = boltscharge;
    }

    public String getPanecharge() {
        return panecharge;
    }

    public void setPanecharge(String panecharge) {
        this.panecharge = panecharge;
    }
}
