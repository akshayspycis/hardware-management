package datamanager.configuration;

import classmanager.configuration.ConfigChargesMgr;
import classmanager.configuration.ConfigMgr;
import classmanager.billmanagement.CustomerBillMgr;
import classmanager.billmanagement.CustomerPaymentMgr;
import classmanager.customermanagement.CustomerProfileMgr;
import classmanager.drivermanagement.DriverProfileMgr;
import classmanager.payroll.EmployeePaymentMgr;
import classmanager.payroll.EmployeeProfileMgr;
import classmanager.expenditure.ExpenditureMgr;
import classmanager.configuration.ConfigItemMgr;
import classmanager.configuration.ConfigNrItemMgr;
import classmanager.configuration.ConfigSizeMgr;
import classmanager.configuration.ConfigStockMgr;
import classmanager.configuration.ConfigUserProfileMgr;
import datamanager.customermanagement.CustomerProfile;
import datamanager.drivermanagement.DriverProfile;
import datamanager.payroll.EmployeeProfile;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import modules.accountmanagement.Login;
import modules.accountmanagement.ManageAccount;
import modules.home.Homepage;
import modules.billmanagement.Add;
import modules.billmanagement.BillPayment;
import modules.billmanagement.NewBill;
import modules.billmanagement.Return;
import modules.billmanagement.ViewBill;
import modules.billmanagement.ViewPayment;
import modules.configuration.Configuration;
import modules.customermanagement.CustomerManagement;
import modules.customermanagement.NewCustomer;
import modules.customermanagement.ViewCustomer;
import modules.drivermanagement.DriverManagement;
import modules.drivermanagement.NewDriver;
import modules.drivermanagement.ViewDriver;
import modules.expenditure.Expenditure;
import modules.payroll.AddEmployee;
import modules.payroll.PayRoll;
import modules.productlicense.ProductLicense;
import modules.stockmanagement.StockManagement;

public class Config {
    
    //database
        static public Connection conn = null;
        public static PreparedStatement pstmt = null;
        public static Statement stmt = null;
        public static ResultSet rs = null;
        public static String sql = null;    
    
    //manager
        public static ConfigMgr configmgr;

        public static ConfigItemMgr configitemmgr;
        public static ConfigSizeMgr configsizemgr;
        public static ConfigNrItemMgr confignritemmgr;
        public static ConfigChargesMgr configchargesmgr;
        public static ConfigStockMgr configstockmgr;
        public static ConfigUserProfileMgr configuserprofilemgr;

        public static CustomerProfileMgr customerprofilemgr;
        public static EmployeeProfileMgr employeeprofilemgr;
        public static DriverProfileMgr driverprofilemgr;

        public static CustomerBillMgr customerbillmgr;
        public static CustomerPaymentMgr customerpaymentmgr;    
        public static EmployeePaymentMgr employeepaymentmgr;    
        public static ExpenditureMgr expendituremgr;    
    
    //forms
        public static Login login;
        public static ManageAccount manageaccount;

        public static NewBill newbill;
        public static ViewBill viewbill;
        public static Add add;
        public static Return retrn;    
        public static BillPayment billpayment;
        public static ViewPayment viewpayment;

        public static Configuration configuration;

        public static CustomerManagement customermanagement;
        public static NewCustomer newcustomer;
        public static ViewCustomer viewcustomer;

        public static DriverManagement drivermanagement;
        public static NewDriver newdriver;
        public static ViewDriver viewdriver;

        public static Expenditure expenditure;

        public static Homepage homepage;

        public static AddEmployee addemployee;
        public static PayRoll payroll;    

        public static ProductLicense productlicense;

        public static StockManagement stockmanagement;
        
    //product config
        public static ArrayList<ConfigItem> configitem = null;
        public static ArrayList<ConfigSize> configsize = null;
        public static ArrayList<ConfigNrItem> confignritem = null;
        public static ArrayList<ConfigCharges> configcharges = null;
        public static ArrayList<ConfigStock> configstock = null;    
        public static ArrayList<ConfigUserProfile> configuserprofile = null;

        public static ArrayList<CustomerProfile> customerprofile = null;
        public static ArrayList<EmployeeProfile> employeeprofile = null;
        public static ArrayList<DriverProfile> driverprofile = null;
}
