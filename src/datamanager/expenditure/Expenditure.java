package datamanager.expenditure;

public class Expenditure {
    
    String expenditureid = null;
    String date = null;
    String time = null;
    String amount = null;
    String description = null;

    public String getExpenditureid() {
        return expenditureid;
    }

    public void setExpenditureid(String expenditureid) {
        this.expenditureid = expenditureid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
