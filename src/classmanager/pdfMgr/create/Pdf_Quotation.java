package classmanager.pdfMgr.create;

import classmanager.pdfMgr.print.PrintPdf;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import datamanager.billmanagement.CustomerBill;
import datamanager.billmanagement.Purchasetable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Pdf_Quotation {

    static SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");    
    static SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy HH:MM a");    
    
    Font df1 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 7, Font.NORMAL);
    Font df2 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 8, Font.NORMAL);
    Font df3 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 8, Font.BOLD);
    
    String string;
    String st1[] = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven","Eight", "Nine"};
    String st2[] = { "Hundred", "Thousand", "Lakh", "Crore"};
    String st3[] = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen","Fifteen", "Sixteen", "Seventeen", "Eighteen", "Ninteen"};
    String st4[] = { "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninty"};
    
    public String createPdf (CustomerBill customerbill) {
        
        String str = null;
        
        try {
            //create folder if not exits
                File file = new File("pdf\\billsummary\\"+sdf1.format(Calendar.getInstance().getTime()));
                if (!file.exists()) {
                    file.mkdirs();
                }
            
            //creation of pdf contents
                String filename = "pdf\\billsummary\\"+sdf1.format(Calendar.getInstance().getTime())+"\\"+customerbill.getCustomername()+" "+customerbill.getBillid()+".pdf";
                OutputStream pdffile = new FileOutputStream(new File(filename));
                Document document = new Document(PageSize.A4_LANDSCAPE.rotate());
                PdfWriter.getInstance(document, pdffile);

                //new line
                    Paragraph clear = new Paragraph(" ", df1);

                    //Quotation Table
                    PdfPTable qtbl = new PdfPTable(2);
                    qtbl.setWidthPercentage(100);
                        
                        //Summary Table
                        PdfPTable tbl = new PdfPTable(1);
                        tbl.setWidthPercentage(100);
                                                
                        PdfPCell c1 = new PdfPCell (new Paragraph("BILL SUMMARY", df2));
                        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                        c1.setBorder(Rectangle.BOTTOM);
                    
                        Image image = Image.getInstance("lib\\head.jpg");
                        image.scaleAbsolute(320f, 55f);//image width,height                        
                    
                        PdfPCell c2 = new PdfPCell (clear);
                        c2.setBorder(0);
                        
                        PdfPCell c3 = new PdfPCell (image);
                        c3.setHorizontalAlignment(Element.ALIGN_CENTER);
                        c3.setFixedHeight(70);
                        c3.setBorder(0);                        
                        
                        //customer & bill details table
                        PdfPTable tbl1 = new PdfPTable(3);
                        tbl1.setWidthPercentage(100);                        

                        try {
                            str = customerbill.getCustomername();
                        } catch (Exception e) {
                            str = "";
                        }                        
                        PdfPCell cd1 = new PdfPCell (new Paragraph("Cus. Name : "+str, df2));
                        cd1.setBorderColor(BaseColor.LIGHT_GRAY);
                        cd1.setColspan(2);

                        try {
                            str = customerbill.getCustomercontactno();
                        } catch (Exception e) {
                            str = "";
                        }
                        PdfPCell cd2 = new PdfPCell (new Paragraph("Con : "+str, df2));
                        cd2.setBorderColor(BaseColor.LIGHT_GRAY);

                        try {
                            str = customerbill.getRecaddress();
                        } catch (Exception e) {
                            str = "";
                        }
                        PdfPCell cd3 = new PdfPCell (new Paragraph("Rec. Address : "+str, df2));
                        cd3.setBorderColor(BaseColor.LIGHT_GRAY);
                        cd3.setColspan(2);

                        try {
                            str = customerbill.getBookingdate();
                        } catch (Exception e) {
                            str = "";
                        }
                        PdfPCell cd4 = new PdfPCell (new Paragraph("B. Date : "+str, df2));
                        cd4.setBorderColor(BaseColor.LIGHT_GRAY);

                        try {
                            str = customerbill.getDrivername();
                        } catch (Exception e) {
                            str = "";
                        }
                        PdfPCell cd5 = new PdfPCell (new Paragraph("Driver : "+str, df2));
                        cd5.setBorderColor(BaseColor.LIGHT_GRAY);

                        try {
                            str = customerbill.getDrivercontactno();
                        } catch (Exception e) {
                            str = "";
                        }
                        PdfPCell cd6 = new PdfPCell (new Paragraph("Con. : "+str, df2));
                        cd6.setBorderColor(BaseColor.LIGHT_GRAY);

                        try {
                            str = customerbill.getVehicleno();
                        } catch (Exception e) {
                            str = "";
                        }
                        PdfPCell cd7 = new PdfPCell (new Paragraph("Veh. No. : "+str, df2));
                        cd7.setBorderColor(BaseColor.LIGHT_GRAY);

                        tbl1.addCell(cd1);
                        tbl1.addCell(cd2);
                        tbl1.addCell(cd3);
                        tbl1.addCell(cd4);
                        tbl1.addCell(cd5);
                        tbl1.addCell(cd6);                    
                        tbl1.addCell(cd7);
                        
                        PdfPCell c4 = new PdfPCell (tbl1);
                        c4.setHorizontalAlignment(Element.ALIGN_CENTER);
                        c4.setPadding(5);
                        c4.setBorder(0);
                        
                        //purchase table
                        PdfPTable tbl2 = new PdfPTable(4);
                        tbl2.setWidthPercentage(100);
                        

                        String[] h2v = {"S. No.", "Item/Size", "Quantity"};
                        for (int i = 0; i < 3; i++) {
                            PdfPCell h = new PdfPCell (new Paragraph(h2v[i], df2));
                            h.setBackgroundColor (new BaseColor (218, 218, 218));
                            h.setHorizontalAlignment (Element.ALIGN_CENTER);
                            h.setBorderColor(BaseColor.LIGHT_GRAY);
                            if (i == 1) {
                                h.setColspan(2);
                            } else {
                                h.setColspan(1);
                            }
                            tbl2.addCell(h);
                        }

                        int quantity1 = 0;                        

                        for (int j = 0; j < 17; j++) {
                            if (j<customerbill.getPurchasetable().size()) {                            
                                Purchasetable pt = customerbill.getPurchasetable().get(j);
                                String[] cv = {
                                    String.valueOf(j+1),
                                    pt.getItem()+" "+pt.getSize(), 
                                    pt.getQuantity(), 
                                    pt.getRate(), 
                                    pt.getAmount()                                
                                };
                                for (int i = 0; i < 3; i++) {
                                    PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                    c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                    c.setVerticalAlignment(Element.ALIGN_CENTER);
                                    c.setBorderColor(new BaseColor (240, 240, 240));
                                    c.setFixedHeight(15);
                                    if (i == 1) {
                                        c.setColspan(2);
                                    } else {
                                        c.setColspan(1);
                                    }
                                    tbl2.addCell(c);
                                }
                                quantity1 = quantity1 + Integer.parseInt(pt.getQuantity());                                

                            } else {
                                String[] cv = {"", "", "", "", ""};
                                for (int i = 0; i < 3; i++) {
                                    PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                    c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                    c.setVerticalAlignment(Element.ALIGN_CENTER);
                                    c.setBorderColor(new BaseColor (240, 240, 240));
                                    c.setFixedHeight(15);
                                    if (i == 1) {
                                        c.setColspan(2);
                                    } else {
                                        c.setColspan(1);
                                    }
                                    tbl2.addCell(c);
                                }
                            }                        
                        }

                        String[] f2v = {"Grant Total ", "", String.valueOf(quantity1)};
                        for (int i = 0; i < 3; i++) {
                            PdfPCell l = new PdfPCell (new Paragraph(f2v[i], df2));
                            l.setBackgroundColor (new BaseColor (218, 218, 218));
                            l.setHorizontalAlignment (Element.ALIGN_CENTER);
                            l.setBorderColor(BaseColor.LIGHT_GRAY);
                            if (i == 1) {
                                l.setColspan(2);
                            } else {
                                l.setColspan(1);
                            }
                            tbl2.addCell(l);
                        }
                        
                        PdfPCell c5 = new PdfPCell (tbl2);
                        c5.setHorizontalAlignment(Element.ALIGN_CENTER);
                        c5.setPadding(5);
                        c5.setBorder(0);                        
                        
                        //footer table
                        PdfPTable tbl5 = new PdfPTable(1);
                        tbl5.setWidthPercentage(100);                        

                        String st = customerbill.getPaidamount();
                        PdfPCell fd0;
                        if (st.equals("")) {
                            fd0 = new PdfPCell (new Paragraph("Advance : 0.0/-", df3));
                        } else {
                            fd0 = new PdfPCell (new Paragraph("Advance : "+customerbill.getPaidamount()+"/-", df3));
                        }                        
                        fd0.setBorder(0);
                        fd0.setHorizontalAlignment (Element.ALIGN_RIGHT);
                        
                        PdfPCell fd1 = new PdfPCell (new Paragraph("NOTE : ", df3));
                        fd1.setBorder(0);
                        
                        PdfPCell fd3 = new PdfPCell (new Paragraph("1. Any alteration to product quality may incure to extra charge to paid by customer.", df2));
                        fd3.setBorder(0);
                        
                        PdfPCell fd5 = new PdfPCell (new Paragraph("2. The disputes, legal matters, court matters, if any, shall be subject to Bhopal jurisdiction only.", df2));
                        fd5.setBorder(0);
                        
                        PdfPCell fd7 = new PdfPCell (new Paragraph("3. Errors & ommision expected.", df2));
                        fd7.setBorder(0);
                        
                        PdfPCell fd8 = new PdfPCell (new Paragraph("Customer Signature", df3));
                        fd8.setHorizontalAlignment (Element.ALIGN_RIGHT);
                        fd8.setBorder(0);

                        PdfPCell fd9 = new PdfPCell (new Paragraph("thanking you for your business...", df2));
                        fd9.setBackgroundColor (new BaseColor (218, 218, 218));
                        fd9.setHorizontalAlignment (Element.ALIGN_CENTER);
                        fd9.setBorderColor(BaseColor.LIGHT_GRAY);
                        
                        tbl5.addCell(fd0);
                        tbl5.addCell(fd1);
                        tbl5.addCell(fd3);
                        tbl5.addCell(fd5);
                        tbl5.addCell(fd7);
                        tbl5.addCell(fd8);
                        tbl5.addCell(fd9);
                        
                        PdfPCell c6 = new PdfPCell (tbl5);
                        c6.setHorizontalAlignment(Element.ALIGN_CENTER);
                        c6.setPadding(5);
                        c6.setBorder(0);
                        
                        tbl.addCell(c1);
                        tbl.addCell(c2);
                        tbl.addCell(c3);
                        tbl.addCell(c4);
                        tbl.addCell(c5);
                        tbl.addCell(c6);
                        
                    PdfPCell qc1 = new PdfPCell (tbl);
                    qc1.setBorder(0);
                        
                    qtbl.addCell(qc1);
                    qtbl.addCell(qc1);
            

            //Now Insert Every Thing Into PDF Document
                document.open();//PDF document opened........
                document.add(qtbl);
                document.close();
                
            //closing of pdf
                pdffile.close();

            return filename;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;            
        }
    }
    
    public boolean printPdf(String filename) {
        try {            
            System.out.println(filename);
            FileInputStream fis = new FileInputStream(filename);
            PrintPdf printPDFFile = new PrintPdf(fis, "New Make Hardware Store");
            printPDFFile.print();

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }  
}
