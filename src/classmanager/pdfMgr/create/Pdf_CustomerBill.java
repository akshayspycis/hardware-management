package classmanager.pdfMgr.create;

import classmanager.pdfMgr.print.PrintPdf;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import datamanager.billmanagement.CustomerBill;
import datamanager.billmanagement.Purchasetable;
import datamanager.billmanagement.Returntable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Ayush Jain
 */
public class Pdf_CustomerBill {
    
    static SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");    
    static SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy HH:MM a");    
    
    Font df1 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 7, Font.NORMAL);
    Font df2 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 8, Font.NORMAL);
    Font df3 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 8, Font.BOLD);
    
    String string;
    String st1[] = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven",
                     "Eight", "Nine", };
    String st2[] = { "Hundred", "Thousand", "Lakh", "Crore" };
    String st3[] = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
                    "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Ninteen", };
    String st4[] = { "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy",
                    "Eighty", "Ninty" };
    
    public String createPdf (CustomerBill customerbill) {
        
        String str = null;
        
        try {
            //create folder if not exits
                File file = new File("pdf\\invoice\\"+sdf1.format(Calendar.getInstance().getTime()));
                if (!file.exists()) {
                    file.mkdirs();
                }
            
            //creation of pdf contents
                String filename = "pdf\\invoice\\"+sdf1.format(Calendar.getInstance().getTime())+"\\"+customerbill.getCustomername()+" "+customerbill.getBillid()+".pdf";
                OutputStream pdffile = new FileOutputStream(new File(filename));
                Document document = new Document();
                PdfWriter.getInstance(document, pdffile);            

                //new line
                    Paragraph clear = new Paragraph(" ", df1);

                //header
                    Chunk header = new Chunk("                                                                                                  CASH/CREDIT MEMO                                                                                                    ", df3);
                    header.setUnderline(0,-2f);//1st co-ordinate is for line width,2nd is space between

                //image
//                    Image image = Image.getInstance("lib\\head.jpg");
//                    image.scaleAbsolute(320f, 55f);//image width,height
//                    image.setAlignment(Element.ALIGN_CENTER);

                //customer & bill details table
                    PdfPTable tbl1 = new PdfPTable(3);
                    tbl1.setWidthPercentage(100);
                    tbl1.setSpacingAfter(15.0f);
                    
                    try {
                        str = customerbill.getCustomername();
                    } catch (Exception e) {
                        str = "";
                    }
                    PdfPCell cd1 = new PdfPCell (new Paragraph("Cus. Name : "+str, df2));
                    cd1.setBorderColor(BaseColor.LIGHT_GRAY);
                    cd1.setColspan(2);

                    try {
                        str = customerbill.getCustomercontactno();
                    } catch (Exception e) {
                        str = "";
                    }
                    PdfPCell cd2 = new PdfPCell (new Paragraph("Con : "+str, df2));
                    cd2.setBorderColor(BaseColor.LIGHT_GRAY);

                    try {
                        str = customerbill.getRecaddress();
                    } catch (Exception e) {
                        str = "";
                    }
                    PdfPCell cd3 = new PdfPCell (new Paragraph("Rec. Address : "+str, df2));
                    cd3.setBorderColor(BaseColor.LIGHT_GRAY);
                    cd3.setColspan(2);
                    
                    try {
                        str = customerbill.getBookingdate();
                    } catch (Exception e) {
                        str = "";
                    }
                    PdfPCell cd4 = new PdfPCell (new Paragraph("B. Date : "+str, df2));
                    cd4.setBorderColor(BaseColor.LIGHT_GRAY);

                    try {
                        str = customerbill.getDrivername();
                    } catch (Exception e) {
                        str = "";
                    }
                    PdfPCell cd5 = new PdfPCell (new Paragraph("Driver : "+str, df2));
                    cd5.setBorderColor(BaseColor.LIGHT_GRAY);

                    try {
                        str = customerbill.getDrivercontactno();
                    } catch (Exception e) {
                        str = "";
                    }
                    PdfPCell cd6 = new PdfPCell (new Paragraph("Con. : "+str, df2));
                    cd6.setBorderColor(BaseColor.LIGHT_GRAY);

                    try {
                        str = customerbill.getVehicleno();
                    } catch (Exception e) {
                        str = "";
                    }
                    PdfPCell cd7 = new PdfPCell (new Paragraph("Veh. No. : "+str, df2));
                    cd7.setBorderColor(BaseColor.LIGHT_GRAY);

                    tbl1.addCell(cd1);
                    tbl1.addCell(cd2);
                    tbl1.addCell(cd3);
                    tbl1.addCell(cd4);
                    tbl1.addCell(cd5);
                    tbl1.addCell(cd6);                    
                    tbl1.addCell(cd7);

                //purchase details table
                    PdfPTable tbl2 = new PdfPTable(10);
                    tbl2.setWidthPercentage(100);
                    tbl2.setSpacingAfter(15.0f);

                    String[] hv = {"S. No.", "Item/Size", "Quantity", "Rate", "Return Date", "No. of Days", "Return Item", "Balance Item", "Amount"};
                    for (int i = 0; i < 9; i++) {
                        PdfPCell h = new PdfPCell (new Paragraph(hv[i], df2));
                        h.setBackgroundColor (new BaseColor (218, 218, 218));
                        h.setHorizontalAlignment (Element.ALIGN_CENTER);
                        h.setBorderColor(BaseColor.LIGHT_GRAY);
                        if (i == 1) {
                            h.setColspan(2);
                        } else {
                            h.setColspan(1);
                        }
                        tbl2.addCell(h);
                    }
                    
                    int quantity1 = 0;
                    float amount1 = 0;

                    int count;                        
                    try {
                        count = customerbill.getReturntable().size();
                    } catch (Exception e) {
                        count = 0;
                    }
                    if (count>0) {
                        for (int j = 0; j < 18; j++) {
                            if (j<customerbill.getReturntable().size()) {
                                Returntable rt = customerbill.getReturntable().get(j);
                                String[] cv = {
                                    String.valueOf(j+1), 
                                    rt.getItem()+" "+rt.getSize(), 
                                    rt.getQuantity(),
                                    rt.getRate(),
                                    rt.getReturndate(), 
                                    rt.getNoofdays(), 
                                    rt.getReturnitem(), 
                                    rt.getBalanceitem(),
                                    rt.getAmount()                                
                                };
                                for (int i = 0; i < 9; i++) {
                                    PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                    c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                    c.setVerticalAlignment(Element.ALIGN_CENTER);
                                    c.setBorderColor(new BaseColor (240, 240, 240));
                                    c.setFixedHeight(15);
                                    if (i == 1) {
                                        c.setColspan(2);
                                    } else {
                                        c.setColspan(1);
                                    }
                                    tbl2.addCell(c);
                                }
                                quantity1 = quantity1 + Integer.parseInt(rt.getQuantity());
                                amount1 = amount1 + Float.parseFloat(rt.getAmount());

                            } else {
                                String[] cv = {"", "", "", "", "", "", "", "", ""};
                                for (int i = 0; i < 9; i++) {
                                    PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                    c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                    c.setVerticalAlignment(Element.ALIGN_CENTER);
                                    c.setBorderColor(new BaseColor (240, 240, 240));
                                    c.setFixedHeight(15);
                                    if (i == 1) {
                                        c.setColspan(2);
                                    } else {
                                        c.setColspan(1);
                                    }
                                    tbl2.addCell(c);
                                }
                            }
                        }
                    } else {
                        for (int j = 0; j < 18; j++) {
                            String[] cv = {"", "", "", "", "", "", "", "", ""};
                            for (int i = 0; i < 9; i++) {
                                PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                c.setVerticalAlignment(Element.ALIGN_CENTER);
                                c.setBorderColor(new BaseColor (240, 240, 240));
                                c.setFixedHeight(15);
                                if (i == 1) {
                                    c.setColspan(2);
                                } else {
                                    c.setColspan(1);
                                }
                                tbl2.addCell(c);
                            }
                        }
                    }                    

                    String[] fv = {"Grant Total ", "", String.valueOf(quantity1), "", "", "", "", "", String.valueOf(amount1)+"/-"};
                    for (int i = 0; i < 9; i++) {
                        PdfPCell l = new PdfPCell (new Paragraph(fv[i], df2));
                        l.setBackgroundColor (new BaseColor (218, 218, 218));
                        l.setHorizontalAlignment (Element.ALIGN_RIGHT);
                        l.setBorderColor(BaseColor.LIGHT_GRAY);
                        if (i == 1) {
                            l.setColspan(2);
                        } else {
                            l.setColspan(1);
                        }
                        tbl2.addCell(l);
                    }
                    
                //pending details table
                    Chunk chunk = new Chunk("Pending Items", df3);
                    
                    PdfPTable tbl3 = new PdfPTable(6);
                    tbl3.setWidthPercentage(100);
                    tbl3.setSpacingAfter(15.0f);

                    String[] h2v = {"S. No.", "Item/Size", "Quantity", "Rate", "Amount"};
                    for (int i = 0; i < 5; i++) {
                        PdfPCell h = new PdfPCell (new Paragraph(h2v[i], df2));
                        h.setBackgroundColor (new BaseColor (218, 218, 218));
                        h.setHorizontalAlignment (Element.ALIGN_CENTER);
                        h.setBorderColor(BaseColor.LIGHT_GRAY);
                        if (i == 1) {
                            h.setColspan(2);
                        } else {
                            h.setColspan(1);
                        }
                        tbl3.addCell(h);
                    }

                    int quantity2 = 0;
                    float amount2 = 0;
                    
                    for (int j = 0; j < 10; j++) {
                        if (j<customerbill.getPurchasetable().size()) {                            
                            Purchasetable pt = customerbill.getPurchasetable().get(j);
                            String[] cv = {
                                String.valueOf(j+1),
                                pt.getItem()+" "+pt.getSize(), 
                                pt.getQuantity(), 
                                pt.getRate(), 
                                pt.getAmount()                                
                            };
                            for (int i = 0; i < 5; i++) {
                                PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                c.setVerticalAlignment(Element.ALIGN_CENTER);
                                c.setBorderColor(new BaseColor (240, 240, 240));
                                c.setFixedHeight(15);
                                if (i == 1) {
                                    c.setColspan(2);
                                } else {
                                    c.setColspan(1);
                                }
                                tbl3.addCell(c);
                            }
                            quantity2 = quantity2 + Integer.parseInt(pt.getQuantity());
                            amount2 = amount2 + Float.parseFloat(pt.getAmount());
                            
                        } else {
                            String[] cv = {"", "", "", "", ""};
                            for (int i = 0; i < 5; i++) {
                                PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                c.setVerticalAlignment(Element.ALIGN_CENTER);
                                c.setBorderColor(new BaseColor (240, 240, 240));
                                c.setFixedHeight(15);
                                if (i == 1) {
                                    c.setColspan(2);
                                } else {
                                    c.setColspan(1);
                                }
                                tbl3.addCell(c);
                            }
                        }                        
                    }
                    
                    String[] f2v = {"Grant Total ", "", String.valueOf(quantity2), "", String.valueOf(amount2)+"/-"};
                    for (int i = 0; i < 5; i++) {
                        PdfPCell l = new PdfPCell (new Paragraph(f2v[i], df2));
                        l.setBackgroundColor (new BaseColor (218, 218, 218));
                        l.setHorizontalAlignment (Element.ALIGN_RIGHT);
                        l.setBorderColor(BaseColor.LIGHT_GRAY);
                        if (i == 1) {
                            l.setColspan(2);
                        } else {
                            l.setColspan(1);
                        }
                        tbl3.addCell(l);
                    }

                //bill details table
                    PdfPTable tbl4 = new PdfPTable(6);
                    tbl4.setWidthPercentage(100);
                    tbl4.setSpacingAfter(15.0f);

                    PdfPCell bd1 = new PdfPCell (new Paragraph("Bolts/Pane Charges : ", df2));
                    bd1.setBorderColor(BaseColor.LIGHT_GRAY);
                    tbl4.addCell(bd1);                    
                    
                    float bc;
                    float pc;
                    try {
                        bc = Float.parseFloat(customerbill.getBoltscharges());
                    } catch (Exception e) {
                        bc = 0.0f;
                    }
                    try {
                        pc = Float.parseFloat(customerbill.getPanecharges());
                    } catch (Exception e) {
                        pc = 0.0f;
                    }                    
                    
                    float bpc = bc + pc;                    
                    
                    PdfPCell bd2 = new PdfPCell (new Paragraph(String.valueOf(bpc), df2));
                    bd2.setBorderColor(BaseColor.LIGHT_GRAY);
                    tbl4.addCell(bd2);
                    
                    PdfPCell bd3 = new PdfPCell (new Paragraph("Transport Charges : ", df2));
                    bd3.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd3);
                    
                    Float tc;
                    try {
                        tc = Float.parseFloat(customerbill.getTransportcharges());
                    } catch (Exception e) {
                        tc = 0.0f;
                    }
                    PdfPCell bd4;                    
                    if (tc == 0.0f) {
                        bd4 = new PdfPCell (new Paragraph("0.0", df2));
                    } else {
                        bd4 = new PdfPCell (new Paragraph(String.valueOf(tc), df2));
                    }                    
                    
                    bd4.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd4);
                    
                    PdfPCell bd5 = new PdfPCell (new Paragraph("Net Amount : ", df2));
                    bd5.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd5);
                    
                    PdfPCell bd6 = new PdfPCell (new Paragraph(customerbill.getTotalamount(), df2));
                    bd6.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd6);
                    
                    PdfPCell bd7 = new PdfPCell (new Paragraph("Discount : ", df2));
                    bd7.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd7);
                    
                    PdfPCell bd8 = new PdfPCell (new Paragraph(customerbill.getDiscount(), df2));
                    bd8.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd8);
                    
                    PdfPCell bd9 = new PdfPCell (new Paragraph("Payable Amount : ", df2));
                    bd9.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd9);
                    
                    PdfPCell bd10 = new PdfPCell (new Paragraph(customerbill.getPayableamount(), df2));
                    bd10.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd10);
                    
                    PdfPCell bd11 = new PdfPCell (new Paragraph("Paid Amount : ", df2));
                    bd11.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd11);
                    
                    PdfPCell bd12 = new PdfPCell (new Paragraph(customerbill.getPaidamount(), df2));
                    bd12.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd12);
                    
                    PdfPCell bd13 = new PdfPCell (new Paragraph("In Words : ", df2));
                    bd13.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd13);
                    
                    int i = (int) Float.parseFloat(customerbill.getPaidamount());
                    String st = convert(i).trim();
                    PdfPCell bd14;
                    if (st.equals("")) {
                        bd14 = new PdfPCell (new Paragraph("NIL", df2));
                    } else {
                        bd14 = new PdfPCell (new Paragraph(st + " Only", df2));
                    }
                    
                    bd14.setBorderColor(BaseColor.LIGHT_GRAY);
                    bd14.setColspan(3);
                    tbl4.addCell(bd14);
                    
                    PdfPCell bd15 = new PdfPCell (new Paragraph("Balance Amount : ", df2));
                    bd15.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd15);
                    
                    PdfPCell bd16 = new PdfPCell (new Paragraph(customerbill.getBalanceamount(), df2));
                    bd16.setBorderColor(BaseColor.LIGHT_GRAY);                    
                    tbl4.addCell(bd16);                
                    
                //footer table
                    PdfPTable tbl5 = new PdfPTable(3);
                    tbl5.setWidthPercentage(100);
                    tbl5.setSpacingAfter(15.0f);

                    PdfPCell fd1 = new PdfPCell (new Paragraph("NOTE : ", df3));
                    fd1.setBorder(0);
                    fd1.setColspan(2);

                    PdfPCell fd2 = new PdfPCell (new Paragraph(" ", df2));
                    fd2.setHorizontalAlignment (Element.ALIGN_CENTER);
                    fd2.setBorder(0);
                    
                    PdfPCell fd3 = new PdfPCell (new Paragraph("1. Any alteration to product quality may incure to extra charge to paid by customer.", df2));
                    fd3.setBorder(0);
                    fd3.setColspan(2);

                    PdfPCell fd4 = new PdfPCell (new Paragraph("NEW MAKE HARDWARE STORE", df3));
                    fd4.setHorizontalAlignment (Element.ALIGN_CENTER);
                    fd4.setBorder(0);
                    
                    PdfPCell fd5 = new PdfPCell (new Paragraph("2. The disputes, legal matters, court matters, if any, shall be subject to Bhopal jurisdiction only.", df2));
                    fd5.setBorder(0);
                    fd5.setColspan(2);

                    PdfPCell fd6 = new PdfPCell (new Paragraph(" ", df2));
                    fd6.setHorizontalAlignment (Element.ALIGN_CENTER);
                    fd6.setBorder(0);
                    
                    PdfPCell fd7 = new PdfPCell (new Paragraph("3. Errors & ommision expected.", df2));
                    fd7.setBorder(0);
                    fd7.setColspan(2);

                    PdfPCell fd8 = new PdfPCell (new Paragraph("Authorised Signatory", df2));
                    fd8.setHorizontalAlignment (Element.ALIGN_CENTER);
                    fd8.setBorder(0);

                    PdfPCell fd9 = new PdfPCell (new Paragraph("thanking you for your business...", df2));
                    fd9.setBackgroundColor (new BaseColor (218, 218, 218));
                    fd9.setHorizontalAlignment (Element.ALIGN_CENTER);
                    fd9.setBorderColor(BaseColor.LIGHT_GRAY);
                    fd9.setColspan(3);

                    tbl5.addCell(fd1);
                    tbl5.addCell(fd2);
                    tbl5.addCell(fd3);
                    tbl5.addCell(fd4);
                    tbl5.addCell(fd5);
                    tbl5.addCell(fd6);
                    tbl5.addCell(fd7);
                    tbl5.addCell(fd8);
                    tbl5.addCell(fd9);                


            //Now Insert Every Thing Into PDF Document
                document.open();//PDF document opened........			       

                document.add(header);
                document.add(clear);
//                document.add(image);
                document.add(clear);
                document.add(tbl1);
                document.add(tbl2);
                document.add(chunk);
                document.add(tbl3);
                document.add(tbl4);
                document.add(tbl5);                

                document.close();
                
            //closing of pdf
                pdffile.close();

            return filename;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;            
        }
    }
    
    public boolean printPdf(String filename) {
        try {
            System.out.println(filename);
            FileInputStream fis = new FileInputStream(filename);
            PrintPdf printPDFFile = new PrintPdf(fis, "New Make Hardware Store");
            printPDFFile.print();

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }   
    
    public String convert(int number) {
                int n = 1;
                int word;
                string = "";
                while (number != 0) {
                        switch (n) {
                        case 1:
                                word = number % 100;
                                pass(word);
                                if (number > 100 && number % 100 != 0) {
                                        show("and ");
                                }
                                number /= 100;
                                break;

                        case 2:
                                word = number % 10;
                                if (word != 0) {
                                        show(" ");
                                        show(st2[0]);
                                        show(" ");
                                        pass(word);
                                }
                                number /= 10;
                                break;

                        case 3:
                                word = number % 100;
                                if (word != 0) {
                                        show(" ");
                                        show(st2[1]);
                                        show(" ");
                                        pass(word);
                                }
                                number /= 100;
                                break;

                        case 4:
                                word = number % 100;
                                if (word != 0) {
                                        show(" ");
                                        show(st2[2]);
                                        show(" ");
                                        pass(word);
                                }
                                number /= 100;
                                break;

                        case 5:
                                word = number % 100;
                                if (word != 0) {
                                        show(" ");
                                        show(st2[3]);
                                        show(" ");
                                        pass(word);
                                }
                                number /= 100;
                                break;
                        }
                        n++;
                }
                return string;
        }

    public void pass(int number) {
            int word, q;
            if (number < 10) {
                    show(st1[number]);
            }
            if (number > 9 && number < 20) {
                    show(st3[number - 10]);
            }
            if (number > 19) {
                    word = number % 10;
                    if (word == 0) {
                            q = number / 10;
                            show(st4[q - 2]);
                    } else {
                            q = number / 10;
                            show(st1[word]);
                            show(" ");
                            show(st4[q - 2]);
                    }
            }
    }

    public void show(String s) {
            String st;
            st = string;
            string = s;
            string += st;
    }    
//    
//    
//    public static void main(String[] args) {
//    
//        Pdf_CustomerBill pm = new Pdf_CustomerBill();
//        pm.createPdf();
//    }
}
