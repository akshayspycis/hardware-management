package classmanager.billmanagement;

import datamanager.configuration.Config;
import datamanager.billmanagement.CustomerPayment;
import java.sql.SQLException;

public class CustomerPaymentMgr {
    
    public boolean insCustomerPayment(CustomerPayment customerpayment) {
        try {
            Config.sql = "insert into customerpayment ("                   
                    + "customerid,"
                    + "date,"
                    + "payment,"
                    + "paidby,"
                    + "balance)"
                    + "values (?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, customerpayment.getCustomerid());
            Config.pstmt.setString(2, customerpayment.getDate());
            Config.pstmt.setString(3, customerpayment.getPayment());
            Config.pstmt.setString(4, customerpayment.getPaidby());            
            Config.pstmt.setString(5, customerpayment.getBalance());            
            
            int x = Config.pstmt.executeUpdate();            
            
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }    
    }    
    
    public boolean delCustomerPayment(String paymentid) {
        int x = 0;
        int y = 0;
        
        String customerid = null;
        String payment = null;
        try {            
            try {
                Config.sql = "Select payment from customerpayment where customerpaymentid = '"+paymentid+"'";
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while(Config.rs.next()){
                    customerid = Config.rs.getString("customerid");
                    payment = Config.rs.getString("payment");
                }            
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            
            Config.sql = "insert into customerpayment ("
                    + "customerid,"                    
                    + "payment)"
                    + "values (?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, customerid);
            Config.pstmt.setString(2, payment);            
            
            x = Config.pstmt.executeUpdate();            
            
            if (x>0) {
                Config.sql = "delete from customerpayment where customerpaymentid = '"+paymentid+"'";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                y = Config.pstmt.executeUpdate();
            }            
            
            if (y>=0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }    
    }
    
}
