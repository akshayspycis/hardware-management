package classmanager.billmanagement;

import datamanager.configuration.Config;
import datamanager.billmanagement.CustomerBill;
import datamanager.billmanagement.Purchasetable;
import datamanager.billmanagement.Returntable;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author AMS
 */
public class CustomerBillMgr {
        
    public boolean insCustomerBill(CustomerBill customerbill) {
        String billid = null;        
        
        try {
            Config.sql = "Select * from customerprofile where "
                    + "name = '"+customerbill.getCustomername()+"' and "
                    + "contactno = '"+customerbill.getCustomercontactno()+"' and "
                    + "address = '"+customerbill.getAddress()+"' and "
                    + "city = '"+customerbill.getCity()+"' and "
                    + "state = '"+customerbill.getState()+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if (!Config.rs.next()) {                    
                Config.sql = "insert into customerprofile (name, contactno, address, city, state) values (?,?,?,?,?)";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                Config.pstmt.setString(1, customerbill.getCustomername());
                Config.pstmt.setString(2, customerbill.getCustomercontactno());
                Config.pstmt.setString(3, customerbill.getAddress());
                Config.pstmt.setString(4, customerbill.getCity());
                Config.pstmt.setString(5, customerbill.getState());
                Config.pstmt.executeUpdate();
            }            
            
            if (!customerbill.getDrivername().equals("") && !customerbill.getDrivercontactno().equals("") && !customerbill.getVehicleno().equals("") ) {                
                Config.sql = "Select * from driverprofile where "
                        + "name = '"+customerbill.getDrivername()+"' and "
                        + "contactno = '"+customerbill.getDrivercontactno()+"' and "
                        + "vehicleno = '"+customerbill.getVehicleno()+"'";                
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if (!Config.rs.next()) {                    
                    Config.sql = "insert into driverprofile (name, contactno, vehicleno) values (?,?,?)";
                    Config.pstmt = Config.conn.prepareStatement(Config.sql);
                    Config.pstmt.setString(1, customerbill.getDrivername());
                    Config.pstmt.setString(2, customerbill.getDrivercontactno());
                    Config.pstmt.setString(3, customerbill.getVehicleno());
                    Config.pstmt.executeUpdate();
                }
            }
            
            Config.sql = "insert into customerbill ("
                    + "customername,"
                    + "customercontactno,"
                    + "address,"
                    + "recaddress,"
                    + "city,"
                    + "state,"
                    + "drivername,"
                    + "drivercontactno,"
                    + "vehicleno,"
                    + "other,"
                    + "transportcharges,"
                    + "bookingdate,"
                    + "bolts,"
                    + "returnbolts,"
                    + "boltscharges,"
                    + "pane,"
                    + "returnpane,"
                    + "panecharges,"
                    + "totalitem,"
                    + "totalreturnitem,"
                    + "totalbalanceitem,"
                    + "totalamount,"
                    + "discount,"
                    + "payableamount,"
                    + "paidamount,"
                    + "balanceamount) "
                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, customerbill.getCustomername());
            Config.pstmt.setString(2, customerbill.getCustomercontactno());
            Config.pstmt.setString(3, customerbill.getAddress());
            Config.pstmt.setString(4, customerbill.getRecaddress());
            Config.pstmt.setString(5, customerbill.getCity());
            Config.pstmt.setString(6, customerbill.getState());
            Config.pstmt.setString(7, customerbill.getDrivername());
            Config.pstmt.setString(8, customerbill.getDrivercontactno());
            Config.pstmt.setString(9, customerbill.getVehicleno());
            Config.pstmt.setString(10, customerbill.getOther());
            Config.pstmt.setString(11, customerbill.getTransportcharges());
            Config.pstmt.setString(12, customerbill.getBookingdate());
            
            Config.pstmt.setString(13, customerbill.getBolts());
            Config.pstmt.setString(14, customerbill.getReturnbolts());
            Config.pstmt.setString(15, customerbill.getBoltscharges());
            
            Config.pstmt.setString(16, customerbill.getPane());
            Config.pstmt.setString(17, customerbill.getReturnpane());
            Config.pstmt.setString(18, customerbill.getPanecharges());
                        
            Config.pstmt.setString(19, customerbill.getTotalitem());
            Config.pstmt.setString(20, customerbill.getTotalreturnitem());
            Config.pstmt.setString(21, customerbill.getTotalbalanceitem());
            Config.pstmt.setString(22, customerbill.getTotalamount());
            Config.pstmt.setString(23, customerbill.getDiscount());
            Config.pstmt.setString(24, customerbill.getPayableamount());
            Config.pstmt.setString(25, customerbill.getPaidamount());
            Config.pstmt.setString(26, customerbill.getBalanceamount());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.sql = "Select max(billid) billid from customerbill";
                Config.rs = Config.stmt.executeQuery(Config.sql);                
                if (Config.rs.next()) {                    
                    billid = Config.rs.getString("billid");
                }
                int i;
                ArrayList<Purchasetable> purchasetable = customerbill.getPurchasetable();
                for (i = 0; i < purchasetable.size(); i++) {
                    Config.sql = "insert into purchasetable (billid,item,size,quantity,rate,amount) values (?,?,?,?,?,?)";
                    Config.pstmt = Config.conn.prepareStatement(Config.sql);
                    Config.pstmt.setString(1, billid);
                    Config.pstmt.setString(2, purchasetable.get(i).getItem());
                    Config.pstmt.setString(3, purchasetable.get(i).getSize());
                    Config.pstmt.setString(4, purchasetable.get(i).getQuantity());
                    Config.pstmt.setString(5, purchasetable.get(i).getRate());
                    Config.pstmt.setString(6, purchasetable.get(i).getAmount());
                    
                    Config.pstmt.executeUpdate();
                }
                
                if (i==purchasetable.size()) {
                    Config.configmgr.loadCustomerProfile();
                    Config.configmgr.loadDriverProfile();
                    Config.configmgr.loadStock();
                    return true;
                } else {
                    return false;
                }                
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean updCustomerBill(CustomerBill customerbill) {
        try {
            Config.sql = "Select * from customerprofile where "
                    + "name = '"+customerbill.getCustomername()+"' and "
                    + "contactno = '"+customerbill.getCustomercontactno()+"' and "
                    + "address = '"+customerbill.getAddress()+"' and "
                    + "city = '"+customerbill.getCity()+"' and "
                    + "state = '"+customerbill.getState()+"'";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            
            if (!Config.rs.next()) {                    
                Config.sql = "insert into customerprofile (name, contactno, address, city, state) values (?,?,?,?,?)";
                
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                Config.pstmt.setString(1, customerbill.getCustomername());
                Config.pstmt.setString(2, customerbill.getCustomercontactno());
                Config.pstmt.setString(3, customerbill.getAddress());
                Config.pstmt.setString(4, customerbill.getCity());
                Config.pstmt.setString(5, customerbill.getState());
                Config.pstmt.executeUpdate();
            }            
            
            if (!customerbill.getDrivername().equals("") && !customerbill.getDrivercontactno().equals("") && !customerbill.getVehicleno().equals("") ) {                
                Config.sql = "Select * from driverprofile where "
                        + "name = '"+customerbill.getDrivername()+"' and "
                        + "contactno = '"+customerbill.getDrivercontactno()+"' and "
                        + "vehicleno = '"+customerbill.getVehicleno()+"'";
                
                Config.rs = Config.stmt.executeQuery(Config.sql);
                if (!Config.rs.next()) {                    
                    Config.sql = "insert into driverprofile (name, contactno, vehicleno) values (?,?,?)";
                    
                    Config.pstmt = Config.conn.prepareStatement(Config.sql);
                    Config.pstmt.setString(1, customerbill.getDrivername());
                    Config.pstmt.setString(2, customerbill.getDrivercontactno());
                    Config.pstmt.setString(3, customerbill.getVehicleno());
                    Config.pstmt.executeUpdate();
                }
            }

            Config.sql = "update customerbill set "
                    + "customername = ?, "
                    + "customercontactno = ?, "
                    + "address = ?, "
                    + "recaddress = ?, "
                    + "city = ?, "
                    + "state = ?, "
                    + "drivername = ?, "
                    + "drivercontactno = ?, "
                    + "vehicleno = ?, "
                    + "other = ?, "
                    + "transportcharges = ?, "
                    + "bookingdate = ?, "                    
                    + "bolts = ?, "
                    + "returnbolts = ?, "
                    + "boltscharges = ?, "                    
                    + "pane = ?, "
                    + "returnpane = ?, "
                    + "panecharges = ?, "                    
                    + "totalitem = ?, "
                    + "totalreturnitem = ?, "
                    + "totalbalanceitem = ?, "
                    + "totalamount = ?, "
                    + "discount = ?, "
                    + "payableamount = ?, "
                    + "paidamount = ?, "
                    + "balanceamount = ? where billid = '"+customerbill.getBillid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, customerbill.getCustomername());
            Config.pstmt.setString(2, customerbill.getCustomercontactno());
            Config.pstmt.setString(3, customerbill.getAddress());
            Config.pstmt.setString(4, customerbill.getRecaddress());
            Config.pstmt.setString(5, customerbill.getCity());
            Config.pstmt.setString(6, customerbill.getState());
            Config.pstmt.setString(7, customerbill.getDrivername());
            Config.pstmt.setString(8, customerbill.getDrivercontactno());
            Config.pstmt.setString(9, customerbill.getVehicleno());
            Config.pstmt.setString(10, customerbill.getOther());
            Config.pstmt.setString(11, customerbill.getTransportcharges());
            Config.pstmt.setString(12, customerbill.getBookingdate());
            
            Config.pstmt.setString(13, customerbill.getBolts());
            Config.pstmt.setString(14, customerbill.getReturnbolts());
            Config.pstmt.setString(15, customerbill.getBoltscharges());
            
            Config.pstmt.setString(16, customerbill.getPane());
            Config.pstmt.setString(17, customerbill.getReturnpane());
            Config.pstmt.setString(18, customerbill.getPanecharges());
                        
            Config.pstmt.setString(19, customerbill.getTotalitem());
            Config.pstmt.setString(20, customerbill.getTotalreturnitem());
            Config.pstmt.setString(21, customerbill.getTotalbalanceitem());
            Config.pstmt.setString(22, customerbill.getTotalamount());
            Config.pstmt.setString(23, customerbill.getDiscount());
            Config.pstmt.setString(24, customerbill.getPayableamount());
            Config.pstmt.setString(25, customerbill.getPaidamount());
            Config.pstmt.setString(26, customerbill.getBalanceamount());
            
            int x = Config.pstmt.executeUpdate();            
                        
            if (x>0) {
                Config.sql = "delete from purchasetable where billid = '"+customerbill.getBillid()+"'";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                Config.pstmt.executeUpdate();
                
                int i;
                ArrayList<Purchasetable> purchasetable = customerbill.getPurchasetable();
                for (i = 0; i < purchasetable.size(); i++) {
                    Config.sql = "insert into purchasetable (billid,item,size,quantity,rate,amount) values (?,?,?,?,?,?)";
                    Config.pstmt = Config.conn.prepareStatement(Config.sql);
                    Config.pstmt.setString(1, customerbill.getBillid());
                    Config.pstmt.setString(2, purchasetable.get(i).getItem());
                    Config.pstmt.setString(3, purchasetable.get(i).getSize());
                    Config.pstmt.setString(4, purchasetable.get(i).getQuantity());
                    Config.pstmt.setString(5, purchasetable.get(i).getRate());
                    Config.pstmt.setString(6, purchasetable.get(i).getAmount());
                    
                    Config.pstmt.executeUpdate();
                }

                Config.sql = "delete from returntable where billid = '"+customerbill.getBillid()+"'";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                Config.pstmt.executeUpdate();

                int j;
                ArrayList<Returntable> returntable = customerbill.getReturntable();
                for (j = 0; j < returntable.size(); j++) {                    
                    Config.sql = "insert into returntable (billid,item,size,quantity,rate,returndate,noofdays,returnitem,balanceitem,amount) values (?,?,?,?,?,?,?,?,?,?)";
                    Config.pstmt = Config.conn.prepareStatement(Config.sql);
                    Config.pstmt.setString(1, customerbill.getBillid());
                    Config.pstmt.setString(2, returntable.get(j).getItem());
                    Config.pstmt.setString(3, returntable.get(j).getSize());
                    Config.pstmt.setString(4, returntable.get(j).getQuantity());
                    Config.pstmt.setString(5, returntable.get(j).getRate());
                    Config.pstmt.setString(6, returntable.get(j).getReturndate());
                    Config.pstmt.setString(7, returntable.get(j).getNoofdays());
                    Config.pstmt.setString(8, returntable.get(j).getReturnitem());
                    Config.pstmt.setString(9, returntable.get(j).getBalanceitem());
                    Config.pstmt.setString(10, returntable.get(j).getAmount());
                    
                    Config.pstmt.executeUpdate();
                }
                
                if (i==purchasetable.size() && j==returntable.size()) {
                    Config.configmgr.loadCustomerProfile();
                    Config.configmgr.loadDriverProfile();
                    Config.configmgr.loadStock();
                    return true;
                } else {
                    return false;                
                }
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean delCustomerBill(String billid) {
        int x,y,z;        
        try {                
            Config.sql = "delete from customerbill where billid = '"+billid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            x = Config.pstmt.executeUpdate();
            
            Config.sql = "delete from purchasetable where billid = '"+billid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            y = Config.pstmt.executeUpdate();
            
            Config.sql = "delete from returntable where billid = '"+billid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            z = Config.pstmt.executeUpdate();

            if (x>=0 && y>=0 && z>=0) {
                Config.configmgr.loadStock();
                return true;                
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }    
    }
}
