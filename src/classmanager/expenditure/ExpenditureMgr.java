package classmanager.expenditure;

import datamanager.configuration.Config;
import datamanager.expenditure.Expenditure;
import java.sql.SQLException;

/**
 *
 * @author AMS
 */
public class ExpenditureMgr {
    
    public boolean insExpenditure(Expenditure expenditure) {
        try {
            Config.sql = "insert into expenditure ("
                    + "date,"
                    + "time," 
                    + "amount,"
                    + "description)"                    
                    + "values (?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, expenditure.getDate());
            Config.pstmt.setString(2, expenditure.getTime());
            Config.pstmt.setString(3, expenditure.getAmount());
            Config.pstmt.setString(4, expenditure.getDescription());
                                    
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean updExpenditure(Expenditure expenditure) {
        try {
            Config.sql = "update expenditure set "
                    + "date = ?, "
                    + "time = ?, "
                    + "amount = ?, "                    
                    + "description = ? where expenditureid = '"+expenditure.getExpenditureid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, expenditure.getDate());
            Config.pstmt.setString(2, expenditure.getTime());
            Config.pstmt.setString(3, expenditure.getAmount());
            Config.pstmt.setString(4, expenditure.getDescription());
                        
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;                
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean delExpenditure(String expenditureid) {
        int x;
        try {                
            Config.sql = "delete from expenditure where expenditureid = '"+expenditureid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }    
    }
}
