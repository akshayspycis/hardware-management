package classmanager.drivermanagement;

import datamanager.configuration.Config;
import datamanager.drivermanagement.DriverProfile;

/**
 *
 * @author AMS
 */
public class DriverProfileMgr {
    
    //method to insert driver profile in database
    public boolean insDriverProfile(DriverProfile driverprofile) {
        try {          
            Config.sql = "insert into driverprofile ("                   
                    + "name,"
                    + "contactno,"
                    + "vehicleno,"
                    + "other)"
                    + "values (?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, driverprofile.getName());
            Config.pstmt.setString(2, driverprofile.getContactno());
            Config.pstmt.setString(3, driverprofile.getVehicleno());
            Config.pstmt.setString(4, driverprofile.getOther());            
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadDriverProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to driver profile in database
   public boolean updDriverProfile(DriverProfile driverprofile) {
        try {
            Config.sql = "update driverprofile set"
                    + " name = ?, "
                    + " contactno = ?, "                    
                    + " vehicleno = ?, "
                    + " other = ? "
                    + " where driverid = '"+driverprofile.getDriverid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, driverprofile.getName());
            Config.pstmt.setString(2, driverprofile.getContactno());
            Config.pstmt.setString(3, driverprofile.getVehicleno());            
            Config.pstmt.setString(4, driverprofile.getOther());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadDriverProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete driver profile in database
   public boolean delDriverProfile(String driverid)
   {
        try {
            Config.sql = "delete from driverprofile where driverid = '"+driverid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadDriverProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
