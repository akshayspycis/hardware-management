package classmanager.customermanagement;

import datamanager.configuration.Config;
import datamanager.customermanagement.CustomerProfile;

/**
 *
 * @author akshay
 */
public class CustomerProfileMgr {
    
    //method to insert coustomerprofile in database
    public boolean insCustomerProfile(CustomerProfile customerprofile) {
        try {          
            Config.sql = "insert into customerprofile ("                   
                    + "name,"
                    + "organization,"
                    + "contactno,"
                    + "address ,"
                    + "city,"                    
                    + "state,"
                    + "other)"
                    + "values (?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, customerprofile.getName());
            Config.pstmt.setString(2, customerprofile.getOrganization());
            Config.pstmt.setString(3, customerprofile.getContactno());
            Config.pstmt.setString(4, customerprofile.getAddress());
            Config.pstmt.setString(5, customerprofile.getCity());
            Config.pstmt.setString(6, customerprofile.getState());
            Config.pstmt.setString(7, customerprofile.getOther());            
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadCustomerProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to coustomerprofile in database 
   public boolean updCustomerProfile(CustomerProfile customerprofile) {
        try {
            Config.sql = "update customerprofile set"
                    + " name = ?, "
                    + " organization = ?, "
                    + " contactno = ?, "                    
                    + " address = ?, "
                    + " city = ?, "
                    + " state = ?, "
                    + " other = ? "
                    + " where customerid = '"+customerprofile.getCustomerid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, customerprofile.getName());
            Config.pstmt.setString(2, customerprofile.getOrganization());
            Config.pstmt.setString(3, customerprofile.getContactno());
            Config.pstmt.setString(4, customerprofile.getAddress());
            Config.pstmt.setString(5, customerprofile.getCity());
            Config.pstmt.setString(6, customerprofile.getState());
            Config.pstmt.setString(7, customerprofile.getOther());
            
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadCustomerProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete coustomerprofile in database
   public boolean delCustomerProfile(String customerid)
   {
        try {          
            Config.sql = "delete from customerprofile where customerid = '"+customerid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadCustomerProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
