package classmanager.payroll;

import datamanager.configuration.Config;
import datamanager.payroll.EmployeePayment;
import java.sql.SQLException;

/**
 *
 * @author AMS
 */
public class EmployeePaymentMgr {

    public boolean insEmployeePayment(EmployeePayment employeepayment) {
        try {              
            Config.sql = "insert into employeepayment ("
                    + "employeeid,"
                    + "month," 
                    + "year,"
                    + "payment,"
                    + "date,"
                    + "time)"                    
                    + "values (?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, employeepayment.getEmployeeid());
            Config.pstmt.setString(2, employeepayment.getMonth());
            Config.pstmt.setString(3, employeepayment.getYear());
            Config.pstmt.setString(4, employeepayment.getPayment());
            Config.pstmt.setString(5, employeepayment.getDate());
            Config.pstmt.setString(6, employeepayment.getTime());
                                    
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadEmployeeProfile();
                return true;
            } else {
                return false;
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean updEmployeePayment(EmployeePayment employeepayment) {
        try {
            Config.sql = "update employeepayment set "
                    + "employeeid = ?, "
                    + "month = ?, "
                    + "year = ?, "
                    + "payment = ?, "
                    + "date = ?, "
                    + "time = ?, "
                    + "other = ? where driverid = '"+employeepayment.getEmployeeid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, employeepayment.getEmployeeid());
            Config.pstmt.setString(2, employeepayment.getMonth());
            Config.pstmt.setString(3, employeepayment.getYear());
            Config.pstmt.setString(4, employeepayment.getPayment());
            Config.pstmt.setString(5, employeepayment.getDate());
            Config.pstmt.setString(6, employeepayment.getTime());            
                        
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadEmployeeProfile();
                return true;
            } else {
                return false;                
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean delEmployeePayment(String paymentid) {
        int x;
        try {                
            Config.sql = "delete from employeepayment where paymentid = '"+paymentid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadEmployeeProfile();
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }    
    }
}
