package classmanager.payroll;

import datamanager.configuration.Config;
import datamanager.payroll.EmployeeProfile;
import java.sql.SQLException;

/**
 *
 * @author AMS
 */
public class EmployeeProfileMgr {
    
    public boolean insEmployeeProfile(EmployeeProfile employeeprofile) {
        try {              
            Config.sql = "insert into employeeprofile ("
                    + "name,"
                    + "fathername," 
                    + "contactno,"
                    + "salary,"
                    + "voterid,"
                    + "address,"
                    + "city,"
                    + "state,"
                    + "joiningdate,"
                    + "leavingdate)"                    
                    + "values (?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, employeeprofile.getName());
            Config.pstmt.setString(2, employeeprofile.getFathername());
            Config.pstmt.setString(3, employeeprofile.getContactno());
            Config.pstmt.setString(4, employeeprofile.getSalary());
            Config.pstmt.setString(5, employeeprofile.getVoterid());
            Config.pstmt.setString(6, employeeprofile.getAddress());
            Config.pstmt.setString(7, employeeprofile.getCity());
            Config.pstmt.setString(8, employeeprofile.getState());
            Config.pstmt.setString(9, employeeprofile.getJoiningdate());
            Config.pstmt.setString(10, employeeprofile.getLeavingdate());
                                    
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadEmployeeProfile();
                return true;
            } else {
                return false;
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean updEmployeeProfile(EmployeeProfile employeeprofile) {
        try {
            Config.sql = "update employeeprofile set "
                    + "name = ?, "
                    + "fathername = ?, "
                    + "contactno = ?, "
                    + "salary = ?, "
                    + "voterid = ?, "
                    + "address = ?, "
                    + "city = ?, "
                    + "state = ?, "
                    + "joiningdate = ?, "
                    + "leavingdate = ? where employeeid = '"+employeeprofile.getEmployeeid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, employeeprofile.getName());
            Config.pstmt.setString(2, employeeprofile.getFathername());
            Config.pstmt.setString(3, employeeprofile.getContactno());
            Config.pstmt.setString(4, employeeprofile.getSalary());
            Config.pstmt.setString(5, employeeprofile.getVoterid());
            Config.pstmt.setString(6, employeeprofile.getAddress());
            Config.pstmt.setString(7, employeeprofile.getCity());
            Config.pstmt.setString(8, employeeprofile.getState());
            Config.pstmt.setString(9, employeeprofile.getJoiningdate());
            Config.pstmt.setString(10, employeeprofile.getLeavingdate());
                        
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadEmployeeProfile();
                return true;
            } else {
                return false;                
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean delEmployeeProfile(String employeeid) {
        int x;
        try {                
            Config.sql = "delete from employeeprofile where employeeid = '"+employeeid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadEmployeeProfile();
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }    
    }
}
