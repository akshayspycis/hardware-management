package classmanager.configuration;

import datamanager.configuration.ConfigCharges;
import datamanager.configuration.Config;

/**
 *
 * @author AMS
 */
public class ConfigChargesMgr {
    
    public boolean updCharges(ConfigCharges charges) {
        try {
            Config.sql = "update configcharges set "
                    + "boltscharge = ?, "
                    + "panecharge = ?";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, charges.getBoltscharge());
            Config.pstmt.setString(2, charges.getPanecharge());            
                        
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadCharges();
                return true;
            } else {
                return false;                
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
