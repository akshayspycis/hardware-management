package classmanager.configuration;

import classmanager.billmanagement.CustomerBillMgr;
import classmanager.billmanagement.CustomerPaymentMgr;
import classmanager.customermanagement.CustomerProfileMgr;
import classmanager.drivermanagement.DriverProfileMgr;
import classmanager.payroll.EmployeePaymentMgr;
import classmanager.payroll.EmployeeProfileMgr;
import classmanager.expenditure.ExpenditureMgr;
import datamanager.configuration.Config;
import datamanager.configuration.ConfigCharges;
import datamanager.customermanagement.CustomerProfile;
import datamanager.drivermanagement.DriverProfile;
import datamanager.payroll.EmployeeProfile;
import datamanager.configuration.ConfigItem;
import datamanager.configuration.ConfigNrItem;
import datamanager.configuration.ConfigSize;
import datamanager.configuration.ConfigStock;
import datamanager.configuration.ConfigUserProfile;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import modules.accountmanagement.Login;
import modules.accountmanagement.ManageAccount;
import modules.home.Homepage;
import modules.billmanagement.Add;
import modules.billmanagement.BillPayment;
import modules.billmanagement.NewBill;
import modules.billmanagement.Return;
import modules.billmanagement.ViewBill;
import modules.billmanagement.ViewPayment;
import modules.configuration.Configuration;
import modules.customermanagement.CustomerManagement;
import modules.customermanagement.NewCustomer;
import modules.customermanagement.ViewCustomer;
import modules.drivermanagement.DriverManagement;
import modules.drivermanagement.NewDriver;
import modules.drivermanagement.ViewDriver;
import modules.expenditure.Expenditure;
import modules.payroll.AddEmployee;
import modules.payroll.PayRoll;
import modules.productlicense.ProductLicense;
import modules.stockmanagement.StockManagement;

public class ConfigMgr {    

    public boolean loadDatabase() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            String userName = "root";
            String password = "root";
            String url = "jdbc:mysql://localhost:3307/newmakehardware";
            Config.conn = DriverManager.getConnection(url, userName, password);
            Config.stmt = Config.conn.createStatement();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean loadManager() {
        try {
            Config.configchargesmgr = new ConfigChargesMgr();
            Config.customerbillmgr = new CustomerBillMgr();
            Config.customerpaymentmgr = new CustomerPaymentMgr();
            Config.customerprofilemgr = new CustomerProfileMgr();
            Config.driverprofilemgr = new DriverProfileMgr();
            Config.employeepaymentmgr = new EmployeePaymentMgr();
            Config.employeeprofilemgr = new EmployeeProfileMgr();
            Config.expendituremgr = new ExpenditureMgr();
            Config.configitemmgr = new ConfigItemMgr();
            Config.confignritemmgr = new ConfigNrItemMgr();
            Config.configsizemgr = new ConfigSizeMgr();
            Config.configstockmgr = new ConfigStockMgr();            
            
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean loadForms() {
        try {
            //makehardware
            Config.login = new Login();
            
            //modules
            Config.homepage = new Homepage();
            Config.configuration = new Configuration(null, true);
            Config.expenditure = new Expenditure(null, true);            
            Config.manageaccount = new ManageAccount(null, true);
            Config.productlicense = new ProductLicense(null, true);
            
            //bill management
            Config.newbill = new NewBill(null, true);
            Config.viewbill = new ViewBill(null, true);
            Config.billpayment = new BillPayment(null, true);
            Config.add = new Add(null, true);
            Config.retrn = new Return(null, true);
            Config.viewpayment = new ViewPayment(null, true);
            
            //customer management
            Config.customermanagement = new CustomerManagement(null, true);
            Config.newcustomer = new NewCustomer(null, true);
            Config.viewcustomer = new ViewCustomer(null, true);
            
            //driver management
            Config.drivermanagement = new DriverManagement(null, true);
            Config.newdriver = new NewDriver(null, true);
            Config.viewdriver = new ViewDriver(null, true);
            
            //payroll            
            Config.addemployee = new AddEmployee(null, true);
            Config.payroll = new PayRoll(null, true);
            
            //stock management
            Config.stockmanagement = new StockManagement(null, true);
                        
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }        
    }    
    
    public boolean loadItem() {
        Config.configitem = new ArrayList<ConfigItem>();
        try {
            Config.sql = "Select * from configitem";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigItem item = new ConfigItem();
                item.setItem(Config.rs.getString("item"));                
                item.setType(Config.rs.getString("type"));                
                Config.configitem.add(item);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadSize() {
        Config.configsize = new ArrayList<ConfigSize>();
        try {            
            Config.sql = "Select * from configsize";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigSize size = new ConfigSize();
                
                size.setSize(Config.rs.getString("size"));
                size.setItem(Config.rs.getString("item"));
                
                Config.configsize.add(size);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadNRItem() {
        Config.confignritem = new ArrayList<ConfigNrItem>();
        try {            
            Config.sql = "Select * from confignritem";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {   
                ConfigNrItem nritem = new ConfigNrItem();                
                
                nritem.setItem(Config.rs.getString("item"));
                nritem.setRate(Config.rs.getString("rate"));
                
                Config.confignritem.add(nritem);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadCharges() {
        Config.configcharges = new ArrayList<ConfigCharges>();
        try {
            Config.sql = "Select * from configcharges";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigCharges configcharges = new ConfigCharges();
                
                configcharges.setBoltscharge(Config.rs.getString("boltscharge"));
                configcharges.setPanecharge(Config.rs.getString("panecharge"));
                Config.configcharges.add(configcharges);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadStock() {
        Config.configstockmgr.updStock();
        Config.configstock = new ArrayList<ConfigStock>();
        try {            
            Config.sql = "Select * from configstock";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigStock stock = new ConfigStock();
                
                stock.setItem(Config.rs.getString("item"));
                stock.setSize(Config.rs.getString("size"));
                stock.setRate(Config.rs.getString("rate"));
                stock.setStock(Config.rs.getString("stock"));
                stock.setAvailable(Config.rs.getString("available"));
                stock.setSold(Config.rs.getString("sold"));
                
                Config.configstock.add(stock);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
        
    public boolean loadUserProfile() {
        Config.configuserprofile = new ArrayList<ConfigUserProfile>();
        try {
            Config.sql = "select * from configuserprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigUserProfile up = new ConfigUserProfile();
                up.setUserid(Config.rs.getString("userid"));
                up.setUsername(Config.rs.getString("username"));
                up.setPassword(Config.rs.getString("password"));
                up.setName(Config.rs.getString("name"));
                up.setContactno(Config.rs.getString("contactno"));
                up.setSecurityquestion(Config.rs.getString("securityquestion"));
                up.setAnswer(Config.rs.getString("answer"));                
                
                Config.configuserprofile.add(up);
            }                        
        } catch (SQLException ex) {
            ex.printStackTrace();            
        }
        if (Config.configuserprofile.size() > 0) {
            return true;
        } else {
            return false;
        }        
    }
    
    public boolean loadCustomerProfile() {
        Config.customerprofile = new ArrayList<CustomerProfile>();
        try {
            Config.sql = "Select * from customerprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                CustomerProfile customerprofile = new CustomerProfile();
                
                customerprofile.setCustomerid(Config.rs.getString("customerid"));
                customerprofile.setName(Config.rs.getString("name"));
                customerprofile.setOrganization(Config.rs.getString("organization"));
                customerprofile.setContactno(Config.rs.getString("contactno"));
                customerprofile.setAddress(Config.rs.getString("address"));
                customerprofile.setCity(Config.rs.getString("city"));
                customerprofile.setState(Config.rs.getString("state"));
                customerprofile.setOther(Config.rs.getString("other"));
                
                Config.customerprofile.add(customerprofile);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;        
    }
    
    public boolean loadEmployeeProfile() {
        Config.employeeprofile = new ArrayList<EmployeeProfile>();        
        
        try {
            Config.sql = "Select * from employeeprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) { 
                
                EmployeeProfile employeeprofile = new EmployeeProfile();
                
                employeeprofile.setEmployeeid(Config.rs.getString("employeeid"));
                employeeprofile.setName(Config.rs.getString("name"));
                employeeprofile.setFathername(Config.rs.getString("fathername"));
                employeeprofile.setContactno(Config.rs.getString("contactno"));
                employeeprofile.setSalary(Config.rs.getString("salary"));
                employeeprofile.setVoterid(Config.rs.getString("voterid"));
                employeeprofile.setAddress(Config.rs.getString("address"));
                employeeprofile.setCity(Config.rs.getString("city"));
                employeeprofile.setState(Config.rs.getString("state"));
                employeeprofile.setJoiningdate(Config.rs.getString("joiningdate"));
                employeeprofile.setLeavingdate(Config.rs.getString("leavingdate"));
                
                Config.employeeprofile.add(employeeprofile);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;        
    }
    
    public boolean loadDriverProfile() {
        Config.driverprofile = new ArrayList<DriverProfile>();
        try {
            Config.sql = "Select * from driverprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                DriverProfile driverprofile = new DriverProfile();
                
                driverprofile.setDriverid(Config.rs.getString("driverid"));
                driverprofile.setName(Config.rs.getString("name"));
                driverprofile.setContactno(Config.rs.getString("contactno"));
                driverprofile.setVehicleno(Config.rs.getString("vehicleno"));
                driverprofile.setOther(Config.rs.getString("other"));
                
                Config.driverprofile.add(driverprofile);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;        
    }
    
}
