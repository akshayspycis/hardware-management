package classmanager.configuration;

import datamanager.configuration.Config;
import datamanager.configuration.ConfigStock;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ConfigStockMgr {

    public boolean insStock(ArrayList<ConfigStock> stock) {
        try {   
            Config.sql = "Delete from configstock";
            System.out.println(Config.sql);
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();            
            int i = 0;
            if (x>=0) {
                for (i = 0; i < stock.size(); i++) {
                    Config.stmt.addBatch("insert into configstock (item, size, rate, stock, available, sold) values ("
                            + "'"+stock.get(i).getItem()+"',"
                            + "'"+stock.get(i).getSize()+"',"
                            + "'"+stock.get(i).getRate()+"',"
                            + "'"+stock.get(i).getStock()+"',"
                            + "'"+stock.get(i).getAvailable()+"',"
                            + "'"+stock.get(i).getSold()+"'"
                            + ")");
                }                
                Config.stmt.executeBatch();
            }
            
            if (i == stock.size()) {
                Config.configmgr.loadStock();                                
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean updStock() {
        String item;
        String size;
        int stk;
        int purchase;
        int retrn;
        
        try {
            Config.sql = "Select * from configstock";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            
            while (Config.rs.next()) {
                String sql;
                ResultSet rs;                
                PreparedStatement pstmt;
                Statement stmt = Config.conn.createStatement();
                
                item = Config.rs.getString("item");
                size = Config.rs.getString("size");
                stk = Integer.parseInt(Config.rs.getString("stock"));
                purchase = 0;
                retrn = 0;
                
                sql = "Select quantity from purchasetable where item = '"+item+"' and size = '"+size+"'";
                rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    purchase = purchase + Integer.parseInt(rs.getString("quantity"));
                }                
                
                sql = "Select returnitem from returntable where item = '"+item+"' and size = '"+size+"'";
                rs = stmt.executeQuery(sql);
                while (rs.next()) {                    
                    retrn = retrn + Integer.parseInt(rs.getString("returnitem"));
                }
                                                
                sql = "update configstock set available = ?, sold = ? where item = '"+item+"' and size = '"+size+"'";
                pstmt = Config.conn.prepareStatement(sql);
                pstmt.setString(1, String.valueOf(stk-(purchase-retrn)));
                pstmt.setString(2, String.valueOf(purchase-retrn));                
                
                pstmt.executeUpdate();                
            }
                        
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
}