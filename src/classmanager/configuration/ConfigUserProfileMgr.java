package classmanager.configuration;

import datamanager.configuration.Config;
import datamanager.configuration.ConfigUserProfile;

public class ConfigUserProfileMgr {

    //method to insert userprofile in database.
    public boolean insUserProfile(ConfigUserProfile userprofile) {
        try {          
            Config.sql = "insert into configuserprofile ("                  
                    + "username,"
                    + "password ,"
                    + "name,"                    
                    + "contactno,"                    
                    + "securityquestion,"
                    + "answer)"
                    
                    + "values (?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, userprofile.getUsername());
            Config.pstmt.setString(2, userprofile.getPassword());
            Config.pstmt.setString(3, userprofile.getName());
            Config.pstmt.setString(4, userprofile.getContactno());
            Config.pstmt.setString(5, userprofile.getSecurityquestion());
            Config.pstmt.setString(6, userprofile.getAnswer());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadUserProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
    //method to update userprofile in database 
    public boolean updUserProfile(ConfigUserProfile userprofile) {
        try {
            Config.sql = "update configuserprofile set "
                    + "username = ?, "
                    + "password = ?, "
                    + "name = ?, "
                    + "contactno = ?, "
                    + "securityquestion = ?, "
                    + "answer = ? "
                  
                    + " where userid = '"+userprofile.getUserid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, userprofile.getUsername());
            Config.pstmt.setString(2, userprofile.getPassword());
            Config.pstmt.setString(3, userprofile.getName());
            Config.pstmt.setString(4, userprofile.getContactno());
            Config.pstmt.setString(5, userprofile.getSecurityquestion());
            Config.pstmt.setString(6, userprofile.getAnswer());
           
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadUserProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
   
    //method to delete userprofile in database
    public boolean delUserProfile(String userid) {
        try {          
            Config.sql = "delete from configuserprofile where userid = '"+userid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadUserProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }    
    } 
    //----------------------------------------------------------------------------------------------
}
