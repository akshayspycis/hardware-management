package classmanager.configuration;

import datamanager.configuration.Config;
import datamanager.configuration.ConfigNrItem;

/**
 *
 * @author AMS
 */
public class ConfigNrItemMgr {
    
    public boolean insNrItem(ConfigNrItem[] nritem) {
        try {   
            Config.sql = "Delete from confignritem";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            
            int i = 0;
            if (x>=0) {                
                for (i = 0; i < nritem.length; i++) {
                    Config.stmt.addBatch("insert into confignritem values ('"+nritem[i].getItem()+"', '"+nritem[i].getRate()+"')");
                }
                Config.stmt.executeBatch();
            }
            
            if (i == nritem.length) {
                Config.configmgr.loadNRItem();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
