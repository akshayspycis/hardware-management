package classmanager.configuration;

import datamanager.configuration.Config;
import datamanager.configuration.ConfigItem;
import java.util.ArrayList;

/**
 *
 * @author AMS
 */
public class ConfigItemMgr {

    public boolean insItem(ArrayList<ConfigItem> item) {
        try {
            Config.sql = "Delete from configitem";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            int i = 0;
            if (x>=0) {                
                for (i = 0; i < item.size(); i++) {
                    Config.stmt.addBatch("insert into configitem values ('"+item.get(i).getItem()+"','"+item.get(i).getType()+"')");
                }
                Config.stmt.executeBatch();
            }
            
            if (i == item.size()) {
                Config.configmgr.loadItem();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}

