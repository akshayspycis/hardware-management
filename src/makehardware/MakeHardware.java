package makehardware;

import classmanager.configuration.ConfigMgr;
import datamanager.configuration.Config;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author AMS
 */
public class MakeHardware {

    Banner banner;
    
    public MakeHardware() {
        banner = new Banner();
        Config.configmgr = new ConfigMgr();
    }
    
    private void showbanner() {        
        banner.setVisible(true);
        banner.setBannerLabel("Checking system configuration...");
    }

    private void connserver() {
        banner.setBannerLabel("Connecting to server...");
    }

    private void conndatabase() {
        banner.setBannerLabel("Checking databse connection...");
        if (!Config.configmgr.loadDatabase()) {
            JOptionPane.showMessageDialog(banner,"Database connectivity problem, product can not be launch.","Some Problem exits.",JOptionPane.ERROR_MESSAGE);            
            System.exit(0);
        }        
    }

    private void initcomponent() {
        banner.setBannerLabel("Initialising components...");
                
        int x=0;
        if (Config.configmgr.loadManager()) {x++;} else {System.out.println("1");}
        if (Config.configmgr.loadUserProfile()){x++;} else {System.out.println("2");}
        if (Config.configmgr.loadNRItem()) {x++;} else {System.out.println("3");}
        if (Config.configmgr.loadItem()) {x++;} else {System.out.println("4");}
        if (Config.configmgr.loadSize()) {x++;} else {System.out.println("5");}        
        if (Config.configmgr.loadCharges()) {x++;} else {System.out.println("6");}
        if (Config.configmgr.loadStock()) {x++;} else {System.out.println("7");}
        if (Config.configmgr.loadCustomerProfile()) {x++;} else {System.out.println("8");}
        if (Config.configmgr.loadEmployeeProfile()) {x++;} else {System.out.println("9");}
        if (Config.configmgr.loadDriverProfile()) {x++;} else {System.out.println("10");}        
        if (Config.configmgr.loadForms()) {x++;} else {System.out.println("11");}
        if (x!=11) {
            JOptionPane.showMessageDialog(banner,"Product initialisation problem, product can not be launch.","Error.",JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    private void hidebanner() {
        banner.setVisible(false);
        Config.login.onloadReset();
        Config.login.setVisible(true);
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MakeHardware.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(MakeHardware.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
                Logger.getLogger(MakeHardware.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
                Logger.getLogger(MakeHardware.class.getName()).log(Level.SEVERE, null, ex);
        }        
        try {            
            MakeHardware obj = new MakeHardware();
            obj.showbanner();        
            Thread.sleep(1000);
            obj.connserver();
            Thread.sleep(1000);
            obj.conndatabase();
            Thread.sleep(1000);
            obj.initcomponent();
            Thread.sleep(1000);
            obj.hidebanner();            
        } catch (InterruptedException ex) {
            Logger.getLogger(MakeHardware.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
